#Project Overview
In the given greentree project we have Desktop view, Mobile view and Tablet View of webpage.
Size of Desktop view is 1440 X 800
Size of Tablet view is 768 X 668
Size of Mobile view is 376 X 667
So, by given all parameters in each view we just make the responsive web page.
Responsive simply means that our web page can be visually appealing in each case as per demand.

#Instruction:-
please operate this in given sizes as we use estimation as per viewports.(viewwidth,viewheight)

#Directory Structure

    ├── README.md
    ├── index.html
    ├── image
    │   └──  abc.png
    └── font
        └──  floodstd.otp
s